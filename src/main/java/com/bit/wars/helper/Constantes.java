package com.bit.wars.helper;

/**
 * {Descricao da Classe}
 *
 * Interface <code>Constantes</code>.
 *
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
public interface Constantes {

	/** The db name. */
	String DB_NAME = "BIT_WARS_DB";

	interface NAMED_QUERY {
		interface PLANETA {
			String FIND_NAME = "Planeta.findByDesc";
		}
	}

	interface PARAMETROS_DB {
		String DB_DIALECT = "mysql";
		String DB_HOST = "localhost";
		String DB_PORT = "3306";
		String DB_NAME = "BIT_WARS_DB";
		String DB_USER = "root";
		String DB_PASSWORD = "";
	}

	interface API {
		String STAR_WARS = "https://swapi.co/api/planets/?search=";
		String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";
		String USER_AGENT = "user-agent";
		String PARAM_HEADERS = "parameters";
	}

	interface CONFIG_BANCO {
		String DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";

		String DIALECT_HIBERNATE = "org.hibernate.dialect.MySQL5Dialect";
	}

	interface STATUS {
		String S_200 = "200";
		String ER_400 = "400";
		String ER_500 = "500";
		String ERRO = "Erro";
	}

	interface ERRO_MSG {
		String BUSCA = "Erro ao solicitar consulta!";
		String GRAVACAO = "Erro ao solicitar gravação!";
		String EXCLUSAO = "Erro ao solicitar exclusão!";
		String ERRO_API_MSG = "Erro ao obter quantidade de aparições em filmes do planeta ";
	}

}
