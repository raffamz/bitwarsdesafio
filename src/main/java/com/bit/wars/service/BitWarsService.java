package com.bit.wars.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bit.wars.dao.PlanetaDAO;
import com.bit.wars.dto.PlanetaDTO;
import com.bit.wars.helper.Constantes;
import com.bit.wars.model.Planeta;

@Service(value = "BitWarsService")
@Transactional(readOnly = false)
public class BitWarsService {

	@Autowired
	private PlanetaDAO planetaDAO;

	@Autowired
	private ApiStarWarsService service;

	private static final Logger LOG = Logger.getLogger(BitWarsService.class);

	public PlanetaDTO savePlaneta(PlanetaDTO request) {
		Planeta planeta = copyProperties(request);
		try {
			planeta.setQtdAparicoesFilmes(service.getQtdParticipacoesFilmes(planeta.getNome()));
		} catch (Exception e) {
			LOG.error(Constantes.ERRO_MSG.ERRO_API_MSG + planeta.getNome());
		}
		planetaDAO.save(planeta);
		return obterDTO(planeta);
	}

	public List<PlanetaDTO> listAllPlaneta() {
		List<Planeta> planetas = planetaDAO.listAll();
		return obterQtdAparicoesFilmes(planetas);
	}

	public List<PlanetaDTO> getNamePlaneta(String nome) {
		List<Planeta> planetas = planetaDAO.findByName(nome);
		return obterQtdAparicoesFilmes(planetas);
	}

	public PlanetaDTO getIdPlaneta(Long id) {
		Planeta p = planetaDAO.findById(id);
		try {
			p.setQtdAparicoesFilmes(service.getQtdParticipacoesFilmes(p.getNome()));
		} catch (Exception e) {
			LOG.error(Constantes.ERRO_MSG.ERRO_API_MSG + p.getNome());
		}
		return obterDTO(p);
	}

	public void deletePlaneta(PlanetaDTO request) {
		Planeta planeta = copyProperties(request);
		planetaDAO.delete(planeta);
	}

	private Planeta copyProperties(PlanetaDTO request) {
		Planeta planeta = new Planeta();
		BeanUtils.copyProperties(request, planeta);
		return planeta;
	}

	private PlanetaDTO obterDTO(Planeta request) {
		PlanetaDTO planeta = new PlanetaDTO();
		BeanUtils.copyProperties(request, planeta);
		return planeta;
	}

	private List<PlanetaDTO> obterQtdAparicoesFilmes(List<Planeta> planetas) {
		List<PlanetaDTO> ps = new ArrayList<PlanetaDTO>();
		for (Planeta p : planetas) {
			PlanetaDTO dto = new PlanetaDTO();
			try {
				p.setQtdAparicoesFilmes(service.getQtdParticipacoesFilmes(p.getNome()));
			} catch (Exception e) {
				LOG.error(Constantes.ERRO_MSG.ERRO_API_MSG + p.getNome());
			}
			BeanUtils.copyProperties(p, dto);
			ps.add(dto);
		}
		return ps;

	}

}