package com.bit.wars.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.bit.wars.dto.PlanetaResponseDTO;
import com.bit.wars.dto.ResponseDTO;
import com.bit.wars.helper.Constantes;
import com.google.gson.Gson;

/**
 * {Descricao da Classe}
 *
 * Classe <code>ServiceHelper</code>.
 *
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
@Component
public class ApiStarWarsService {

	public Integer getQtdParticipacoesFilmes(final String planeta) throws Exception {

		RestTemplate restTemplate = new RestTemplate();
		Gson gson = new Gson();

		HttpEntity<String> entity = obterRequestEntity();

		ResponseEntity<String> response = restTemplate.exchange(Constantes.API.STAR_WARS + planeta, HttpMethod.GET,
				entity, String.class);
		ResponseDTO dto = gson.fromJson(response.getBody(), ResponseDTO.class);
		return obtemQtdFilmes(planeta, dto);
	}

	private HttpEntity<String> obterRequestEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add(Constantes.API.USER_AGENT, Constantes.API.USER_AGENT_VALUE);
		HttpEntity<String> entity = new HttpEntity<String>(Constantes.API.PARAM_HEADERS, headers);
		return entity;
	}

	private Integer obtemQtdFilmes(final String planeta, ResponseDTO dto) {
		if (dto.getCount() < 0) {
			return 0;
		} else if (dto.getCount() > 1) {
			return verificaEqualNome(dto.getResults(), planeta);
		} else {
			return dto.getResults().get(0).getFilms().size();
		}
	}

	private Integer verificaEqualNome(List<PlanetaResponseDTO> lista, String nome) {
		for (PlanetaResponseDTO dto : lista) {
			if (dto.getName().equals(nome)) {
				return dto.getFilms().size();
			}
		}
		return 0;
	}

}
