
package com.bit.wars.exception;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

import com.bit.wars.dto.MensagemDTO;

/**
 * {Descricao da Classe}
 * 
 * Classe <code>ExtracaoSorteioException</code>.
 * 
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
public class DesafioBitWarsException  extends Exception implements Serializable {

	/** Constante serialVersionUID. */
	private static final long serialVersionUID = -2838021031046304638L;

	/** Constante LOG. */
	private static final Logger LOG = Logger.getLogger(DesafioBitWarsException.class);


	/**
	 * Instancia um(a) novo(a) processa venda exception.
	 * 
	 * @param msg
	 *            msg
	 */
	public DesafioBitWarsException(final MensagemDTO msg) {
		super(msg.toString());
		LOG.error(msg.toString());

	}
	public DesafioBitWarsException(final MensagemDTO msg, final Exception e) {
		String msgErro = this.retornaMsgErro(msg.toString(), e);
		LOG.error(msgErro);

	}

	/**
	 * Instancia um(a) novo(a) processa venda exception.
	 * 
	 * @param msg
	 *            msg
	 */
	public DesafioBitWarsException(final String msg) {
		super(msg);
		LOG.error(msg);
	}

	/**
	 * Instancia um(a) novo(a) processa venda exception.
	 * 
	 * @param msg
	 *            msg
	 * @param e
	 *            e
	 */
	public DesafioBitWarsException(final String msg, final Exception e) {
		super(msg);
		String msgErro = this.retornaMsgErro(msg, e);
		LOG.error(msgErro);

	}

	/**
	 * Retorna msg erro.
	 * 
	 * @param msg
	 *            msg
	 * @param e
	 *            e
	 * @return string
	 */
	public String retornaMsgErro(final String msg, final Exception e) {

		StringBuilder sb = new StringBuilder();
		sb.append(" [Data]: ");
		sb.append(new Date());
		sb.append("\n");
		int nivel = 1;
		for (StackTraceElement l : e.getStackTrace()) {
			if (l.getClassName().contains("br.com.brasilcap.")) {
				sb.append(" [Nivel]: ");
				sb.append(nivel);
				sb.append(" [Classe]: ");
				sb.append(l.getClassName());
				sb.append(" [Metodo]: ");
				sb.append(l.getMethodName());
				sb.append(" [Linha]: ");
				sb.append(l.getLineNumber());
				sb.append("\n");
				nivel += 1;
			}

		}

		sb.append(" ");
		sb.append("[Exception]: ");
		sb.append(e);
		sb.append("\n");
		sb.append("[Mensagem]: ");
		sb.append(msg);

		return sb.toString();
	}

}
