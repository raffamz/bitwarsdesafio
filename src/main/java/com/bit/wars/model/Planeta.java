/**
 * Copyright (c) 2017  Brasilcap - www.brasilcap.com.br
 * Todos os direitos reservados.
 *
 * N�O ALTERE OU REMOVA AS INFORMA��ES DE COPYRIGHT
 * OU AS INFORMA��ES CONTIDAS NESTE HEADER
 *
 * Este c�digo-fonte � de propriedade da Brasilcap Capitaliza��es S.A.
 * e n�o pode ser copiado, modificado ou compartilhado sem autoriza��o
 * pr�via, estando sujeito a penalidades judiciais caso ocorra.
 *
 */
package com.bit.wars.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the BIT_PLANETA_TB database table.
 *
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
@Entity
@Table(name = "BIT_PLANETA_TB")
public class Planeta implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PLANETA")
	private Long idPlaneta;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "CLIMA")
	private String clima;

	@Column(name = "TERRENO")
	private String terreno;
	
	@Transient
	private int qtdAparicoesFilmes;

	public Long getIdPlaneta() {
		return idPlaneta;
	}

	public void setIdPlaneta(Long idPlaneta) {
		this.idPlaneta = idPlaneta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getClima() {
		return clima;
	}

	public void setClima(String clima) {
		this.clima = clima;
	}

	public String getTerreno() {
		return terreno;
	}

	public void setTerreno(String terreno) {
		this.terreno = terreno;
	}

	public int getQtdAparicoesFilmes() {
		return qtdAparicoesFilmes;
	}

	public void setQtdAparicoesFilmes(int qtdAparicoesFilmes) {
		this.qtdAparicoesFilmes = qtdAparicoesFilmes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}