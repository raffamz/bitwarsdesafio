package com.bit.wars.controller;

import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bit.wars.dto.MensagemDTO;
import com.bit.wars.dto.PlanetaDTO;
import com.bit.wars.helper.Constantes;
import com.bit.wars.service.BitWarsService;

/**
 * @author raffamz
 * @version 1.0 (08/08/2018)
 *
 */
@RestController
@Scope("request")
@CrossOrigin(origins = "*")
public class BitWarsController {

	@Autowired
	private BitWarsService bitWarsService;

	private static final Logger LOG = Logger.getLogger(BitWarsController.class);

	@RequestMapping(value = "/planetas", method = RequestMethod.POST, produces = {
			javax.ws.rs.core.MediaType.APPLICATION_JSON })
	public ResponseEntity<?> post(@RequestBody PlanetaDTO request) {
		try {
			request = bitWarsService.savePlaneta(request);
		} catch (Exception e) {
			LOG.error(Constantes.ERRO_MSG.GRAVACAO, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<PlanetaDTO>(request, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/planetas", method = RequestMethod.GET, produces = {
			javax.ws.rs.core.MediaType.APPLICATION_JSON })
	public ResponseEntity<?> getListAll(@QueryParam("nome") String nome) {
		List<PlanetaDTO> planetas = null;
		System.out.println(nome);
		try {
			if (StringUtils.isNotBlank(nome)) {
				planetas = bitWarsService.getNamePlaneta(nome);
			} else {
				planetas = bitWarsService.listAllPlaneta();
			}
			if (planetas.isEmpty()) {
				return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			LOG.error(Constantes.ERRO_MSG.BUSCA, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<PlanetaDTO>>(planetas, HttpStatus.OK);
	}

	@RequestMapping(value = "/planetas/{id}", method = RequestMethod.GET, produces = {
			javax.ws.rs.core.MediaType.APPLICATION_JSON })
	public ResponseEntity<?> getId(@PathVariable("id") Long id) {
		PlanetaDTO planeta = null;
		try {
			planeta = bitWarsService.getIdPlaneta(id);
			if (planeta == null) {
				return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			LOG.error(Constantes.ERRO_MSG.BUSCA, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<PlanetaDTO>(planeta, HttpStatus.OK);
	}

	@RequestMapping(value = "/planetas/{id}", method = RequestMethod.DELETE, produces = {
			javax.ws.rs.core.MediaType.APPLICATION_JSON })
	public ResponseEntity<?> delete(@PathParam("id") Long id) {
		PlanetaDTO dto = new PlanetaDTO();
		dto.setIdPlaneta(id);
		try {
			bitWarsService.deletePlaneta(dto);
		} catch (Exception e) {
			LOG.error(Constantes.ERRO_MSG.EXCLUSAO, e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<MensagemDTO>(new MensagemDTO(":(", "Planeta destruído !"), HttpStatus.OK);
	}

}
