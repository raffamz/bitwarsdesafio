package com.bit.wars.dto;

/**
 *
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
public class PlanetaDTO {

	private Long idPlaneta;

	private String nome;

	private String clima;

	private String terreno;

	private int qtdAparicoesFilmes;

	public Long getIdPlaneta() {
		return idPlaneta;
	}

	public void setIdPlaneta(Long idPlaneta) {
		this.idPlaneta = idPlaneta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getClima() {
		return clima;
	}

	public void setClima(String clima) {
		this.clima = clima;
	}

	public String getTerreno() {
		return terreno;
	}

	public void setTerreno(String terreno) {
		this.terreno = terreno;
	}

	public int getQtdAparicoesFilmes() {
		return qtdAparicoesFilmes;
	}

	public void setQtdAparicoesFilmes(int qtdAparicoesFilmes) {
		this.qtdAparicoesFilmes = qtdAparicoesFilmes;
	}

}