

package com.bit.wars.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * {Descricao da Classe}
 *
 * Classe <code>AppContext</code>.
 *
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
@Configuration
public class AppContext extends WebMvcConfigurationSupport {

}
