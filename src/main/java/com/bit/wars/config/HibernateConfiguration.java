
package com.bit.wars.config;

import java.util.Properties;
import java.util.TimeZone;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.bit.wars.helper.Constantes;

/**
 * {Descricao da Classe}
 *
 * Classe <code>HibernateConfiguration</code>.
 *
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
@Configuration
@Component
@EnableTransactionManagement
@ComponentScan("com.bit.wars")
public class HibernateConfiguration {

	/**
	 * Session factory.
	 *
	 * @return local session factory bean
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		TimeZone timeZone = TimeZone.getTimeZone("America/Sao_Paulo");
		TimeZone.setDefault(timeZone);

		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setPackagesToScan(new String[] { "com.bit.wars" });
		sessionFactory.setHibernateProperties(this.hibernateProperties());
		return sessionFactory;
	}

	/**
	 * Hibernate properties.
	 *
	 * @return properties
	 */
	private Properties hibernateProperties() {
		Properties properties = new Properties();

		properties.put("hibernate.connection.driver_class", Constantes.CONFIG_BANCO.DRIVER_CLASS_NAME);
		properties.put("hibernate.connection.url", retornaURL());
		properties.put("hibernate.connection.username", Constantes.PARAMETROS_DB.DB_USER);
		properties.put("hibernate.connection.password", Constantes.PARAMETROS_DB.DB_PASSWORD);
		properties.put("hibernate.dialect", Constantes.CONFIG_BANCO.DIALECT_HIBERNATE);

		properties.put("hibernate.show_sql", false);
		properties.put("hibernate.format_sql", false);
		properties.put("hibernate.hbm2ddl.auto", "validate");
		properties.put("hibernate.connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
		properties.put("hibernate.c3p0.min_size", "5");
		properties.put("hibernate.c3p0.max_size", "600");
		properties.put("hibernate.c3p0.timeout", "1800");
		properties.put("hibernate.c3p0.max_statements", "500");
		properties.put("hibernate.c3p0.preferredTestQuery", "select 1 from dual");
		properties.put("hibernate.c3p0.maxConnectionAge", "3600");
		properties.put("hibernate.c3p0.testConnectionOnCheckin", "false");
		properties.put("hibernate.c3p0.testConnectionOnCheckout", "true");
		properties.put("hibernate.c3p0.acquireRetryDelay", "1000");
		properties.put("hibernate.c3p0.acquireRetryAttempts", "30");
		properties.put("hibernate.c3p0.breakAfterAcquireFailure", "false");
		properties.put("hibernate.c3p0.idleConnectionTestPeriod", "1000");
		properties.put("hibernate.c3p0.maxIdleTime", "1800");
		properties.put("hibernate.c3p0.maxStatements", "500");
		properties.put("hibernate.c3p0.checkoutTimeout", "1000");

		return properties;
	}

	private String retornaURL() {
		StringBuilder sb = new StringBuilder();
		sb.append("jdbc:");
		sb.append(Constantes.PARAMETROS_DB.DB_DIALECT);
		sb.append("://");
		sb.append(Constantes.PARAMETROS_DB.DB_HOST);
		sb.append(":");
		sb.append(Constantes.PARAMETROS_DB.DB_PORT);
		sb.append("/");
		sb.append(Constantes.PARAMETROS_DB.DB_NAME);

		return sb.toString();
	}

	/**
	 * Transaction manager.
	 *
	 * @param s
	 *            s
	 * @return hibernate transaction manager
	 */
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(final SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}
}