
package com.bit.wars.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 * {Descricao da Classe}
 *
 * Classe <code>Config</code>.
 *
 * @author raffamz
 * @version 1.0 (08/08/2018)
 */
@Configuration
@ComponentScan("com.bit.wars")
@EnableWebMvc
public class Config extends WebMvcConfigurerAdapter {

	/**
	 * Setup view resolver.
	 *
	 * @return the url based view resolver
	 */
	@Bean
	public UrlBasedViewResolver setupViewResolver() {

		final UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".html");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}
}
