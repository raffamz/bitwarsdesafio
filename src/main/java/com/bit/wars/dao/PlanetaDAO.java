/**
 *
 */
package com.bit.wars.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bit.wars.dao.generic.HibernateDAOGenerico;
import com.bit.wars.model.Planeta;

/**
 * @author raffamz
 * @version 1.0 (08/08/2018)
 *
 */
@Repository
public class PlanetaDAO extends HibernateDAOGenerico<Planeta, Long>
		implements IPlanetaDAO<Planeta, Long>, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List<Planeta> findByName(String nome) {
		Criteria criteria = this.getSessionFactory().getCurrentSession().createCriteria(Planeta.class);
		criteria.add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE));
		return criteria.list();
	}

}
