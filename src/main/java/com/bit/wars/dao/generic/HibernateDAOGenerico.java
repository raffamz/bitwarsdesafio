
package com.bit.wars.dao.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * {Descricao da Classe}
 * 
 * Classe <code>HibernateDAOGenerico</code>.
 * 
 * @author raffamz
 * @version 1.0 (08/08/2018)
 * @param <T>
 *            o tipo genérico
 * @param <ID>
 *            o tipo genérico
 */
public abstract class HibernateDAOGenerico<T, ID extends Serializable> implements GenericDAO<T, ID> {

	/** session factory. */
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	/**
	 * Define session factory.
	 * 
	 * @param sessionFactory
	 *            define session factory
	 */
	public void setSessionFactory(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Obtem session factory.
	 * 
	 * @return session factory
	 */
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.brasilcap.processavenda.dao.generic.GenericDAO#findById(java.io.
	 * Serializable)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T findById(final ID id) {
		return (T) this.getCurrentSession().get(this.getTypeClass(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.brasilcap.processavenda.dao.generic.GenericDAO#listAll()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<T> listAll() {
		return this.getCurrentSession().createCriteria(this.getTypeClass().getName())
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.asc("id")).list();
	}

	/**
	 * Cria.
	 * 
	 * @param entity
	 *            entity
	 */
	public void create(final T entity) throws com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException, SQLException{
		this.getCurrentSession().persist(entity);
		this.getCurrentSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.brasilcap.processavenda.dao.generic.GenericDAO#save(java.lang.Object)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T save(final T entity) {
		T t = (T) this.getCurrentSession().save(entity);
		this.getCurrentSession().flush();
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.brasilcap.processavenda.dao.generic.GenericDAO#merge(java.lang.Object)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T merge(final T entity) {
		T t = (T) this.getCurrentSession().merge(entity);
		this.getCurrentSession().flush();

		return t;
	}

	/**
	 * Update.
	 * 
	 * @param entity
	 *            entity
	 */
	public void update(final T entity) {
		this.getCurrentSession().update(entity);
		this.getCurrentSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.brasilcap.processavenda.dao.generic.GenericDAO#saveOrUpdate(java.lang.
	 * Object)
	 */
	@Override
	public void saveOrUpdate(final T entity) {
		this.getCurrentSession().saveOrUpdate(entity);
		this.getCurrentSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.brasilcap.processavenda.dao.generic.GenericDAO#delete(java.lang.
	 * Object)
	 */
	@Override
	public void delete(final T entity) {
		this.getCurrentSession().delete(entity);
		this.getCurrentSession().flush();
	}

	/**
	 * Obtem current session.
	 * 
	 * @return current session
	 */
	public final Session getCurrentSession() {
		return this.sessionFactory.getCurrentSession();
	}

	/**
	 * Obtem type class.
	 * 
	 * @return type class
	 */
	private Class<?> getTypeClass() {
		Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		return clazz;
	}
}