
package com.bit.wars.dao.generic;


import java.io.Serializable;
import java.util.List;

/**
 * {Descricao da Classe}
 * 
 * Interface <code>GenericDAO</code>.
 * 
 * @author raffamz
 * @version 1.0 (08/08/2018)
 * @param <T>
 *            o tipo genérico
 * @param <ID>
 *            o tipo genérico
 */
public interface GenericDAO<T, ID extends Serializable> {

	/**
	 * Find by id.
	 * 
	 * @param id
	 *            id
	 * @return t
	 */
	T findById(ID id);

	/**
	 * List all.
	 * 
	 * @return list
	 */
	List<T> listAll();

	/**
	 * Merge.
	 * 
	 * @param entity
	 *            entity
	 * @return t
	 */
	T merge(T entity);

	/**
	 * Save or update.
	 * 
	 * @param entity
	 *            entity
	 */
	void saveOrUpdate(T entity);

	/**
	 * Save.
	 * 
	 * @param entity
	 *            entity
	 * @return t
	 */
	T save(T entity);

	/**
	 * Delete.
	 * 
	 * @param entity
	 *            entity
	 */
	void delete(T entity);
}