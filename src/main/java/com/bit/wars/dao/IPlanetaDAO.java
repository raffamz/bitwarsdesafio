/**
 *
 */
package com.bit.wars.dao;

import java.io.Serializable;

import com.bit.wars.dao.generic.GenericDAO;

/**
 * @author raffamz
 * @version 1.0 (08/08/2018)
 *
 */
public interface IPlanetaDAO <Planeta, ID extends Serializable> extends GenericDAO<Planeta, Long> {

}
