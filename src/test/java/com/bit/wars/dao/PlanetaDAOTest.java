package com.bit.wars.dao;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.bit.wars.config.Config;
import com.bit.wars.config.HibernateConfiguration;
import com.bit.wars.model.Planeta;

/**
 * @author raffamz
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { HibernateConfiguration.class, Config.class })
@WebAppConfiguration
public class PlanetaDAOTest {

	/** Constante LOG. */
	private static final Logger LOG = Logger.getLogger(PlanetaDAOTest.class);

	@Autowired
	private PlanetaDAO planetaDAO;

	@Test
	public void testSavePlanetaInvalido() {
		try {
			planetaDAO.create(new Planeta());
			Assert.fail("testSavePlanetaInvalido Falhou - Salvou planeta invalido");
		} catch (Exception e) {
			LOG.error("Erro ao criar entity");
			return;
		}
	}

	@Test
	public void testUpdatePlanetaInvalido() {
		try {
			planetaDAO.update(new Planeta());
			Assert.fail("testSavePlanetaInvalido Falhou - Salvou planeta invalido");
		} catch (Exception e) {
			LOG.error("Erro ao atualizar entity");
			return;
		}
	}

}
